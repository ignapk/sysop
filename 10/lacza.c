#include <sys/stat.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>

int main ()
{
    int n;
    scanf ("%d", &n);
    int p[2];
    pipe (p);
    for (int i = 0; i < n; i++)
    {
        if (fork () == 0)
        {
          close (p[0]);
          int k;
          scanf ("%d", &k);
          write (p[1], &k, sizeof (int));
          exit (0);
        }
        wait (0);
    }
    close (p[1]);
    srand (time (NULL));
    int l = rand () % 100 + 1;
    printf ("Losowa: %d\n", l);
    int tmp;
    for (int i = 0; i < n; i++)
    {
      read (p[0], &tmp, sizeof (tmp));
      printf ("%d ", tmp);
      if (tmp == l) printf ("rowne\n");
      else printf ("rozne\n");
    }
      
    return 0;
}
