#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

pthread_mutex_t myMutex = PTHREAD_MUTEX_INITIALIZER;

void *pierwszy (void *vargp)
{
  pthread_mutex_lock (&myMutex);
  int a = *((int *)vargp);
  int b = *((int *)vargp + 1);
  printf ("%d + %d = %d\n", a, b, a + b);
  pthread_mutex_unlock (&myMutex);
}

void *drugi (void *vargp)
{
  pthread_mutex_lock (&myMutex);
  int a = *((int *)vargp);
  int b = *((int *)vargp + 1);
  printf ("%d - %d = %d\n", a, b, a - b);
  pthread_mutex_unlock (&myMutex);
}

void *trzeci (void *vargp)
{
  pthread_mutex_lock (&myMutex);
  int a = *((int *)vargp);
  int b = *((int *)vargp + 1);
  printf ("%d * %d = %d\n", a, b, a * b);
  pthread_mutex_unlock (&myMutex);
}

int main ()
{
  pthread_t pierwszy_id;
  pthread_t drugi_id;
  pthread_t trzeci_id;
  int args[2];
  scanf ("%d%d", args, args + 1);
  pthread_create (&pierwszy_id, NULL, pierwszy, args);
  pthread_create (&drugi_id, NULL, drugi, args);
  pthread_join (pierwszy_id, NULL);
  pthread_join (drugi_id, NULL);
  pthread_create (&trzeci_id, NULL, trzeci, args);
  pthread_join (trzeci_id, NULL);
  return 0;
}
  
