#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/wait.h>

int main ()
{
  if (fork () == 0)
  {
    char buff[100];
    scanf ("%s", buff);
    int fd = open ("plik", O_RDWR| O_CREAT, 0640);
    write (fd, buff, strlen (buff));
    if (fork () == 0)
    {
      char buff2[100];
      scanf ("%s", buff2);
      int fd2 = open ("plik", O_APPEND | O_CREAT, 0640);
      write (fd, buff2, strlen (buff2));
      if (fork () == 0)
      {
        int fd3 = open ("plik", O_RDONLY);
        char buff3[100];
        int i;
        for (i = 0; read (fd3, buff3 + i, 1); i++);
        printf ("%s\n%d\n", buff3, i);
        close (fd3);
        exit (0);
      }
      close (fd2);
      wait (NULL);
      exit (0);
    }
    close (fd);
    wait (NULL);
    exit (0);
  }
  wait (NULL);
  return 0;
}
