#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/shm.h>
#include <sys/ipc.h>

int main ()
{
    int shmid = shmget (21337, 4 * sizeof (int), IPC_CREAT | 0600);
    int *buff = shmat (shmid, NULL, 0);
    printf ("%d", *buff + *(buff + 1) + *(buff + 2) + *(buff + 3));
    return 0;
}
