#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/shm.h>
#include <sys/ipc.h>

int main ()
{
    int shmid = shmget (21337, 4 * sizeof (int), IPC_CREAT | 0600);
    int *buf = shmat (shmid, NULL, 0);
    scanf ("%d %d", buf, buf + 1);
    return 0;
}
