#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

int main ()
{
	int fd = open ("Lorem", O_APPEND | O_CREAT, 0640);
	if (fd == -1)
	{
		printf ("Error\n");
		return 1;
	}
	else
	{
		char buff[] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.\n";
		for (int i = 0; i<15; i++)
			write (fd, buff, sizeof (buff));
		close (fd);
	}
	return 0;
}

