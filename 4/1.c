#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

int main ()
{
	printf ("PID=%d, PPID=%d\n", getpid (), getppid ());
	if (fork () == 0)
	{
		if (fork () == 0)
		{
			sleep (1);
			printf ("PID=%d, PPID=%d\n", getpid (), getppid ());
			exit (0);
		}
		printf ("PID=%d, PPID=%d\n", getpid (), getppid ());
		exit (0);
	}
	if (fork () == 0)
	{
		if (fork () == 0)
		{
			sleep (1);
			printf ("PID=%d, PPID=%d\n", getpid (), getppid ());
			exit (0);
		}
		printf ("PID=%d, PPID=%d\n", getpid (), getppid ());
		exit (0);
	}
	wait (NULL);
	if (fork () == 0)
	{
		sleep (1);
		printf ("PID=%d, PPID=%d\n", getpid (), getppid ());
		exit (0);
	}
}
