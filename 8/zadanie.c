#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

pthread_mutex_t myMutex = PTHREAD_MUTEX_INITIALIZER;

void *watekPierwszy (void *vargp)
{
  pthread_mutex_lock (&myMutex);
  char buff[100];
  for (int i = 0; read (0, buff + i, 1); i++);
  int fd = open ("plik", O_RDWR | O_CREAT, 0640);
  write (fd, buff, strlen (buff));
  pthread_mutex_unlock (&myMutex);
}

int main ()
{
  pthread_t thread_id;
  pthread_create (&thread_id, NULL, watekPierwszy, NULL);
  pthread_join (thread_id, NULL);
  pthread_mutex_lock (&myMutex);
  int fd = open ("plik", O_RDWR);
  char buff[100];
  int lines = 0;
  for (int i = 0; read (fd, buff + i, 1); i++)
    if (*(buff + i) == '\n')
      lines++;
      
  write (1, buff, strlen (buff));
  printf ("%d", lines);
  pthread_mutex_unlock (&myMutex);
}
