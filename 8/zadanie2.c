#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

int main ()
{
  int p[2];
  char napis1[] = "Hello ";
  char napis2[] = "World\n";
  pipe (p);
  
  if (fork () == 0)
  {
    close (p[0]);
    write (p[1], napis1, strlen (napis1) + 1);
    exit (0);
  }
  if (fork () == 0)
  {
    close (p[0]);
    write (p[1], napis2, strlen (napis2) + 1);
    exit (0);
  }
  else
  {
    close (p[1]);
    char buff[10];
    read (p[0], buff, sizeof (buff));
    printf ("Odczytano: %s", buff);
  }
  return 0;
}

