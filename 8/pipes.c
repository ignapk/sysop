#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

int main ()
{
  int p[2];
  char napis[] = "Hello\n";
  pipe (p);
  
  if (fork () == 0)
  {
    close (p[0]);
    write (p[1], napis, strlen (napis) + 1);
    exit (0);
  }
  else
  {
    close (p[1]);
    char buff[10];
    read (p[0], buff, sizeof (buff));
    printf ("Odczytano: %s", buff);
  }
  return 0;
}

