#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define MAX 256

int main(){
    int p[2];
    if(pipe(p)==-1){
        perror("pipe error");
        exit(1);
    }
    switch(fork()){
    case -1:
        perror("process error");
        exit(1);
    case 0:
        close(p[0]);
        dup2(p[1],1);
        execlp("ls","ls",NULL);
        perror("ls error");
        exit(1);

    default:
        char buf[MAX];
        int lb, i;
        close(p[1]);
        while((lb=read(p[0], buf, MAX))>0){
            for(i=0; i<lb; i++)
                buf[i]=toupper(buf[i]);
            if(write(1,buf,lb)==-1){
                perror("display error");
                exit(1);
            }
            if(lb==-1){
                perror("pipe read error");
                exit(1);
            }
        }
    }
}
