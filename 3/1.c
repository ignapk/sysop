#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main ()
{
	printf ("Before forking\n");
	printf ("getpid=%d, getppid=%d\n", getpid (), getppid ());
	int ret = fork ();
	printf ("After forking, ret=%d\n", ret);
	printf ("getpid=%d, getppid=%d\n", getpid (), getppid ());
	return 0;
}
