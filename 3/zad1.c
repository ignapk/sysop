#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

int main ()
{
	printf ("Rodzic getpid=%d\n", getpid ());
	if (fork () == 0)
	{
		//sleep (1);
		printf ("Syn1 getpid=%d, getppid=%d\n", getpid (), getppid ());
		if (fork () == 0)
		{
			//sleep (1);
			printf ("Wnuk1 getpid=%d, getppid=%d\n", getpid (), getppid ());
			exit (0);
		}
		//wait (NULL);
		exit (0);
	}
	if (fork () == 0)
	{
		//sleep (1);
		printf ("Syn2 getpid=%d, getppid=%d\n", getpid (), getppid ());
		if (fork () == 0)
		{
			//sleep (1);
			printf ("Wnuk2 getpid=%d, getppid=%d\n", getpid (), getppid ());
			exit (0);
		}
		//wait (NULL);
		exit (0);

	}
	printf ("BOTTOM TEKST\n");
	wait (NULL);
}
