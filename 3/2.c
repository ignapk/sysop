#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

int main ()
{
	printf ("Matka PID=%d\n", getpid ());
	if (fork () == 0)
	{
		sleep(1);
		printf ("Potomek1\ngetpid=%d, getppid=%d\n", getpid (), getppid ());
		exit (0);
	}
	if (fork () == 0)
	{
		sleep(1);
		printf ("Potomek2\ngetpid=%d, getppid=%d\n", getpid (), getppid ());
		exit (0);

	}
	printf ("BOTTOM TEKST\n");
	wait (NULL);
}
