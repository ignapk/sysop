#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

int main ()
{
	if (fork () == 0)
	{
		execl ("/bin/ls", "ls", "-l", NULL);
		exit (0);
	}
	if (fork () == 0)
	{
		execlp ("ls", "ls", "-l", NULL);
		exit (0);
	}
	char *arr[] = {"ls","-l",NULL};
	if (fork () == 0)
	{
		execv ("/bin/ls", arr);
		exit (0);
	}
	if (fork () == 0)
	{
		execvp ("ls", arr);
		exit (0);
	}
	wait (NULL);
}
