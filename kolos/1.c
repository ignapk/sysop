#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
int main ()
{
        if (fork () == 0)
        {
	    int p1 = open ("P1.txt",  O_RDWR | O_CREAT, 0640);
	    int p2 = open ("P2.txt",  O_RDWR | O_CREAT, 0640);
                if (fork () == 0)
                {
		char buff[15];
		read (0, buff, sizeof (buff));  
		write (p1, buff, sizeof (buff));
		exit (0);
                }
	    if (fork () == 0)
  	    {
		int p1cpy = dup (p1);
		char buff2[15];
		read (p1cpy, buff2, sizeof (buff2));
		printf ("%s\n", buff2);
		close (p1cpy);
		exit (0);
	    }
	    
		wait (NULL);
		close (p1);
		close (p2);
                exit (0);
        } 
        if (fork () == 0)
        {
	    int p1 = open ("P1.txt",  O_RDWR, 0640);
	    int p2 = open ("P2.txt",  O_RDWR, 0640);
	    char buff3[15];
	    read (p1, buff3, sizeof (buff3));
	    write (p2, buff3, sizeof (buff3));
	    if (fork () == 0)
	    {
		char buff4[15];
		lseek (p2, 10, SEEK_SET);
		read (p2, buff4, sizeof (buff4));
		printf ("%s\n", buff4);
		exit (0);
	    }
	    if (fork () == 0)
	    {
		if (access ("P2.txt", X_OK))
			chmod ("P2.txt", 0770);
		exit (0);
	    }
	    wait (NULL);
                close (p1);
                close (p2);

                exit (0);
        }
        if (fork () == 0)
        {
	if (fork () == 0) exit (0);
	if (fork () == 0) exit (0);
        }
        wait (NULL);
}

