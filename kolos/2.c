#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

int main ()
{
	if (fork () == 0)
	{
		execlp ("pwd", "", NULL);
		exit (0);
	}
	if (fork () == 0)
	{
		execlp ("ps", "a", "u", "x", NULL);
		exit (0);
	}
}
