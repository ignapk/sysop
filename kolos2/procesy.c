#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/wait.h>

int main ()
{
  // Potomek 1
  if (fork () == 0)
  {
    int fd1 = open ("plik1", O_WRONLY | O_CREAT, 0640);
    // Potomek potomka 1
    if (fork () == 0)
    {
      char buff[100];
      scanf ("%s", buff);
      write (fd1, buff, strlen (buff));
      exit (0);
    }
    wait (NULL);
    close (fd1);
    exit (0);
  }
  wait (NULL);
  // Potomek 2
  if (fork () == 0)
  {
    char buff[100];
    int fd1 = open ("plik1", O_RDONLY, 0640);
    int i;
    for (i = 0; read (fd1, buff + i, 1); i++);
    int fd2 = open ("plik2", O_WRONLY | O_CREAT, 0640);
    write (fd2, buff, i); 
    // Potomek potomka 2
    if (fork () == 0)
    {
      if (!access ("plik2", R_OK)) printf ("Read access\n");
      if (!access ("plik2", W_OK)) printf ("Write access\n");
      if (!access ("plik2", X_OK)) printf ("Execute access\n"); 
    }
    close (fd1);
    close (fd2);
    wait (NULL);
    exit (0);
  }
  // Proces macierzysty
  int fd1 = open ("plik1", O_RDONLY, 0640);
  int i;
  char tmp;
  for (i = 0; read (fd1, &tmp, 1); i++);
  printf ("%d\n", i);
  close (fd1);
  return 0;
}
