#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>

int main ()
{
	const char *p = "katalog1/plik1";
	if (!access (p, R_OK))
		printf ("read access\n");
	if (!access (p, W_OK))
	{
		printf ("write access, removing...\n");
		chmod (p, 0440);
	}	
	else
	{
		printf ("no write access, adding...\n");
		chmod (p, 0660);
	}

	if (!access (p, X_OK))
	{
		printf ("execute access\n");
	}
	return 0;
}

