#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>

int main ()
{
	char buff[10];
	read (0, buff, sizeof (buff));
	buff[sizeof (buff) - 1] = '\0';
	for (int i = 0; i < sizeof (buff); i++)
	       buff[i] = toupper (buff[i]);	
	write (1, buff, sizeof (buff));
	return 0;
}

