#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

int main ()
{
	int in = open ("katalog1/katalog2/plik2", O_RDONLY);
	int out = open ("katalog1/plik1", O_WRONLY);
	if (in == -1 || out == -1)
	{
		printf ("Error\n");
		return 1;
	}
	else
	{
		char buff[10];
		read (in, buff, sizeof (buff));
		write (out, buff, sizeof (buff));
		close (in);
		close (out);
	}
	return 0;
}
