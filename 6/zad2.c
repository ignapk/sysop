#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

int main ()
{
	int fd = open ("katalog1/katalog2/plik2", O_WRONLY | O_CREAT, 0640);
	if (fd == -1)
	{
		printf ("Error\n");
		return 1;
	}
	else
	{
		char buff[10];
		read (0, buff, sizeof (buff));
		buff[sizeof (buff) - 1]='\0';
		write (fd, buff, sizeof (buff));
		close (fd);
	}
	return 0;
}

